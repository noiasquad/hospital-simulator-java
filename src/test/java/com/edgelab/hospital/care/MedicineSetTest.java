package com.edgelab.hospital.care;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static com.edgelab.hospital.care.Medicine.Insulin;
import static org.assertj.core.api.Assertions.assertThat;

public class MedicineSetTest {

    @Test
    public void should_return_all_medicines() {
        assertThat(MedicineSet.full()).containsExactlyInAnyOrder(Medicine.values());
    }

    @Test
    public void should_return_no_medicine() {
        assertThat(MedicineSet.empty()).hasSize(0);
    }

    @Test
    public void should_return_any_mixture_of_medicines() {
        //when
        var result = MedicineSet.anySubsets();
        //then
        assertThat(result).hasSize((int) Math.pow(2, Medicine.values().length));
        assertThat(result).doesNotHaveDuplicates();
    }

    @Test
    public void should_return_any_mixture_of_medicines_but_insulin() {
        //when
        var result = MedicineSet.allSubsetsNotHaving(Set.of(Insulin));
        //then
        assertThat(result).hasSize((int) Math.pow(2, Medicine.values().length - 1));
        assertThat(result).doesNotHaveDuplicates();
    }

    @Test
    public void should_return_any_mixture_of_medicines_including_insulin() {
        //when
        var result = MedicineSet.allSubsetsHaving(Set.of(Insulin));
        //then
        assertThat(result).hasSize((int) Math.pow(2, Medicine.values().length - 1));
        assertThat(result).doesNotHaveDuplicates();
    }
}

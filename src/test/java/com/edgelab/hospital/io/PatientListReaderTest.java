package com.edgelab.hospital.io;

import com.edgelab.hospital.care.HealthStatus;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.edgelab.hospital.care.HealthStatus.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class PatientListReaderTest {

    //given
    private PatientListReader reader = new PatientListReader();

    @Test
    public void should_parse_valid_single_patient_accordingly() {
        //when-then
        for (var state : HealthStatus.values()) {
            assertThat(reader.read(state.getAcronym())).isEqualTo(List.of(state));
        }
    }

    @Test
    public void should_parse_valid_multi_patient_list_accordingly() {
        //when-then
        assertThat(reader.read(" F,H,D,T,X, F,H,D,T,X ")).isEqualTo(
                List.of(
                        Fever, Healthy, Diabetes, Tuberculosis, Dead,
                        Fever, Healthy, Diabetes, Tuberculosis, Dead
                ));
    }

    @Test
    public void should_parse_empty_patient_list_accordingly() {
        //when-then
        assertThat(reader.read("")).isEqualTo(List.of());
    }

    @Test
    public void should_detect_illegal_syntax_for_list() {
        //when-then
        assertThatThrownBy(() -> reader.read("F,")).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> reader.read("F;H")).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void should_detect_unknown_health_status() {
        //when-then
        assertThatThrownBy(() -> reader.read("a")).isInstanceOf(IllegalArgumentException.class);
    }
}

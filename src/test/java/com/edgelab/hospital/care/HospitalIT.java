package com.edgelab.hospital.care;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static com.edgelab.hospital.care.HealthStatus.*;
import static com.edgelab.hospital.care.Medicine.*;
import static com.edgelab.hospital.care.MedicineSet.allSubsetsHaving;
import static com.edgelab.hospital.care.MedicineSet.allSubsetsNotHaving;
import static org.assertj.core.api.Assertions.assertThat;

public class HospitalIT {

    private Hospital hospital = new Hospital();

    @Test
    public void should_make_patient_healthy_with_the_right_medicine() {
        for (var mixture : MedicineSet.allSubsets(Set.of(Aspirin), Set.of(Aspirin, Paracetamol))) {
            assertThat(hospital.careWith(Fever, mixture)).isEqualTo(Healthy);
        }
        for (var mixture : MedicineSet.allSubsets(Set.of(Paracetamol), Set.of(Aspirin, Paracetamol))) {
            assertThat(hospital.careWith(Fever, mixture)).isEqualTo(Healthy);
        }
        for (var mixture : MedicineSet.allSubsets(Set.of(Antibiotic), Set.of(Aspirin, Paracetamol))) {
            assertThat(hospital.careWith(Tuberculosis, mixture)).isEqualTo(Healthy);
        }
    }

    @Test
    public void should_let_patient_survive_with_insulin() {
        for (var mixture : MedicineSet.allSubsets(Set.of(Insulin), Set.of(Paracetamol, Aspirin))) {
            assertThat(hospital.careWith(Diabetes, mixture)).isEqualTo(Diabetes);
        }
    }

    @Test
    public void should_care_more_than_patient_at_once() {
        //given
        var patients = List.of(Diabetes, Diabetes);
        //when
        var result = hospital.careWith(patients, Set.of(Insulin));
        //then
        assertThat(result).hasSize(patients.size());
        assertThat(result).isEqualTo(List.of(Diabetes, Diabetes));
    }

    @Test
    public void should_let_patient_remain_sick_with_the_wrong_medicine() {
        for (var mixture : allSubsetsNotHaving(Set.of(Aspirin, Paracetamol))) {
            assertThat(hospital.careWith(Fever, mixture)).isEqualTo(Fever);
        }
        for (var mixture : allSubsetsNotHaving(Set.of(Antibiotic, Aspirin, Paracetamol))) {
            assertThat(hospital.careWith(Tuberculosis, mixture)).isEqualTo(Tuberculosis);
        }
        for (var mixture : allSubsetsNotHaving(Set.of(Antibiotic, Paracetamol))) {
            assertThat(hospital.careWith(Tuberculosis, mixture)).isEqualTo(Tuberculosis);
        }
        for (var mixture : allSubsetsNotHaving(Set.of(Antibiotic, Aspirin))) {
            assertThat(hospital.careWith(Tuberculosis, mixture)).isEqualTo(Tuberculosis);
        }
    }

    @Test
    public void should_make_healthy_patient_catch_fever_with_insulin_and_antibiotic() {
        for (var mixture : MedicineSet.allSubsets(Set.of(Insulin, Antibiotic), Set.of(Aspirin, Paracetamol))) {
            assertThat(hospital.careWith(Healthy, mixture)).isEqualTo(Fever);
        }
    }

    @Test
    public void should_not_resurrect_dead_patient_with_any_mixture_of_medicine() {
        for (var mixture : MedicineSet.anySubsets()) {
            assertThat(hospital.careWith(Dead, mixture)).isEqualTo(Dead);
        }
    }

    @Test
    public void should_kill_patient_with_paracetamol_and_aspirin() {
        for (var mixture : allSubsetsHaving(Set.of(Paracetamol, Aspirin))) {
            assertThat(hospital.careWith(Fever, mixture)).isEqualTo(Dead);
            assertThat(hospital.careWith(Healthy, mixture)).isEqualTo(Dead);
            assertThat(hospital.careWith(Diabetes, mixture)).isEqualTo(Dead);
            assertThat(hospital.careWith(Tuberculosis, mixture)).isEqualTo(Dead);
        }
    }

    @Test
    public void should_kill_patient_suffering_diabetes_without_insulin() {
        for (var mixture : allSubsetsNotHaving(Set.of(Insulin))) {
            assertThat(hospital.careWith(Diabetes, mixture)).isEqualTo(Dead);
        }
    }

}

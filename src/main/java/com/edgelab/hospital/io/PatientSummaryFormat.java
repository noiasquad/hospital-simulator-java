package com.edgelab.hospital.io;

import com.edgelab.hospital.care.HealthStatus;

import java.util.LinkedHashMap;
import java.util.List;

import static java.util.stream.Collectors.*;

public class PatientSummaryFormat {

    public String format(List<HealthStatus> data) {

        var statistic = List.of(HealthStatus.values()).stream()
                .collect(toMap(
                        HealthStatus::getAcronym,
                        it -> 0L,
                        (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new));

        statistic.putAll(data.stream()
                .collect(groupingBy(HealthStatus::getAcronym, counting())));

        return statistic.entrySet()
                .stream()
                .map(e -> String.format("%s:%d", e.getKey(), e.getValue()))
                .collect(joining(","));
    }
}

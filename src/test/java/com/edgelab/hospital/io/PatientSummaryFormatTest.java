package com.edgelab.hospital.io;

import org.junit.jupiter.api.Test;

import java.util.List;

import static com.edgelab.hospital.care.HealthStatus.*;
import static org.assertj.core.api.Assertions.assertThat;

public class PatientSummaryFormatTest {

    //given
    private PatientSummaryFormat formatter = new PatientSummaryFormat();

    @Test
    public void should_format_multi_patient_list_accordingly() {
        //when-then
        assertThat(formatter.format(List.of())).isEqualTo("F:0,H:0,D:0,T:0,X:0");
        assertThat(formatter.format(List.of(Fever))).isEqualTo("F:1,H:0,D:0,T:0,X:0");
        assertThat(formatter.format(List.of(Fever, Fever))).isEqualTo("F:2,H:0,D:0,T:0,X:0");
        assertThat(formatter.format(List.of(Fever, Healthy, Diabetes, Tuberculosis, Dead))).isEqualTo("F:1,H:1,D:1,T:1,X:1");
        assertThat(formatter.format(List.of(Fever, Healthy, Healthy, Healthy, Dead))).isEqualTo("F:1,H:3,D:0,T:0,X:1");
    }
}

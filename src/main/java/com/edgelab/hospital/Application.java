package com.edgelab.hospital;

import com.edgelab.hospital.care.Hospital;
import com.edgelab.hospital.care.Medicine;
import com.edgelab.hospital.io.MedicineSetReader;
import com.edgelab.hospital.io.PatientListReader;
import com.edgelab.hospital.io.PatientSummaryFormat;

import java.util.Set;

public class Application {

    public static void main(String... args) {
        try {
            if (args.length == 0) {
                throw new IllegalArgumentException("Patient list must be provided");
            } else {
                var patients = new PatientListReader().read(args[0]);
                var medicines = args.length > 1 ? new MedicineSetReader().read(args[1]) : Set.<Medicine>of();
                print(new PatientSummaryFormat().format(new Hospital().careWith(patients, medicines)));
            }
        } catch (Exception e) {
            print(e.toString());
        }
    }

    private static void print(String text) {
        System.out.println(text);
    }
}

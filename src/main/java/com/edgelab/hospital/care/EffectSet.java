package com.edgelab.hospital.care;

import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import static com.edgelab.hospital.care.EffectSet.EligibleEffect.effectOf;
import static com.edgelab.hospital.care.HealthStatus.*;
import static com.edgelab.hospital.care.Medicine.*;

@AllArgsConstructor(staticName = "of")
public class EffectSet {

    @NonNull
    private List<EligibleEffect> mortalEffectList;
    @NonNull
    private List<EligibleEffect> safeEffectList;

    public static EffectSet defaultSet() {
        return new EffectSet(
                List.of(
                        effectOf(Set.of(), m -> m.containsAll(Set.of(Paracetamol, Aspirin)), state -> Dead),
                        effectOf(Set.of(Diabetes), m -> !m.contains(Insulin), state -> Dead)
                ),
                List.of(
                        effectOf(Set.of(Healthy), m -> m.containsAll(Set.of(Antibiotic, Insulin)), state -> Fever),
                        effectOf(Set.of(Fever), m -> m.contains(Aspirin), state -> Healthy),
                        effectOf(Set.of(Fever), m -> m.contains(Paracetamol), state -> Healthy),
                        effectOf(Set.of(Tuberculosis), m -> m.contains(Antibiotic), state -> Healthy)
                )
        );
    }

    public UnaryOperator<HealthStatus> electEffect(HealthStatus state, Set<Medicine> medicineSet) {
        return electEffect(mortalEffectList, state, medicineSet)
                .orElseGet(() -> electEffect(safeEffectList, state, medicineSet).orElseGet(UnaryOperator::identity));
    }

    private Optional<UnaryOperator<HealthStatus>> electEffect(List<EligibleEffect> effectList, HealthStatus state, Set<Medicine> medicineSet) {
        return effectList.stream()
                .map(it -> it.apply(state, medicineSet))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst();
    }

    interface EligibleEffect extends BiFunction<HealthStatus, Set<Medicine>, Optional<UnaryOperator<HealthStatus>>> {

        static EligibleEffect effectOf(Set<HealthStatus> states, Predicate<Set<Medicine>> medicines, UnaryOperator<HealthStatus> effect) {
            return (s, m) -> (states.isEmpty() || states.contains(s)) && medicines.test(m) ? Optional.of(effect) : Optional.empty();
        }
    }
}

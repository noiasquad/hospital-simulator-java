package com.edgelab.hospital.care;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import java.util.List;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum Medicine {
    Aspirin("As"),
    Antibiotic("An"),
    Insulin("I"),
    Paracetamol("P");

    private static final List<Medicine> LOV = List.of(values());
    @Getter
    @NonNull
    private String acronym;

    public static Medicine fromAcronym(String acronym) {
        return LOV.stream()
                .filter(it -> it.acronym.equalsIgnoreCase(acronym))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unknown medicine: %s", acronym)));
    }
}


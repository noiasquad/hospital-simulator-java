package com.edgelab.hospital.io;

import com.edgelab.hospital.care.HealthStatus;

import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

public class PatientListReader {

    private static final Pattern PATIENT_LIST_PATTERN = Pattern.compile("^(\\s*[A-Za-z]*)(\\s*,\\s*[A-Za-z]+\\s*)*$");
    private static final Predicate<String> PATIENT_LIST_MATCHER = PATIENT_LIST_PATTERN.asMatchPredicate();

    public List<HealthStatus> read(String input) {
        if (PATIENT_LIST_MATCHER.test(input)) {
            return parse(input);
        } else {
            throw new IllegalArgumentException("Illegal syntax of patient list");
        }
    }

    private List<HealthStatus> parse(String input) {
        return List.of(input.split(",")).stream()
                .filter(it -> !it.isEmpty())
                .map(String::strip)
                .map(HealthStatus::fromAcronym)
                .collect(toList());
    }
}

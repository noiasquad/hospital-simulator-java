package com.edgelab.hospital.care;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

public class Hospital {

    private EffectSet effectSet = EffectSet.defaultSet();

    public HealthStatus careWith(HealthStatus patient, Set<Medicine> medicines) {
        return effectSet.electEffect(patient, medicines).apply(patient);
    }

    public List<HealthStatus> careWith(List<HealthStatus> patientList, Set<Medicine> medicines) {
        return patientList.stream().map(it -> careWith(it, medicines)).collect(toList());
    }
}

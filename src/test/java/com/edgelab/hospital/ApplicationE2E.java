package com.edgelab.hospital;

import com.edgelab.hospital.care.Hospital;
import com.edgelab.hospital.io.MedicineSetReader;
import com.edgelab.hospital.io.PatientListReader;
import com.edgelab.hospital.io.PatientSummaryFormat;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ApplicationE2E {

    @Test
    public void diabetic_patients_die_without_insulin() {
        //given
        var patients = new PatientListReader().read("D,D");
        var medicines = new MedicineSetReader().read("");
        //when
        var result = new PatientSummaryFormat().format(new Hospital().careWith(patients, medicines));
        //when-then
        assertThat(result).isEqualTo("F:0,H:0,D:0,T:0,X:2");
    }

    @Test
    public void paracetamol_cures_fever() {
        //given
        var patients = new PatientListReader().read("F");
        var medicines = new MedicineSetReader().read("P");
        //when
        var result = new PatientSummaryFormat().format(new Hospital().careWith(patients, medicines));
        //when-then
        assertThat(result).isEqualTo("F:0,H:1,D:0,T:0,X:0");
    }
}

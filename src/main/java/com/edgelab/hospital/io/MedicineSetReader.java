package com.edgelab.hospital.io;

import com.edgelab.hospital.care.Medicine;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toSet;

public class MedicineSetReader {

    private static final Pattern MEDICINE_LIST_PATTERN = Pattern.compile("^(\\s*[A-Za-z]*)(\\s*,\\s*[A-Za-z]+\\s*)*$");
    private static final Predicate<String> MEDICINE_LIST_MATCHER = MEDICINE_LIST_PATTERN.asMatchPredicate();

    public Set<Medicine> read(String input) {
        if (MEDICINE_LIST_MATCHER.test(input)) {
            return parse(input);
        } else {
            throw new IllegalArgumentException("Illegal syntax of medicine list");
        }
    }

    private Set<Medicine> parse(String input) {
        return List.of(input.split(",")).stream()
                .filter(it -> !it.isEmpty())
                .map(String::strip)
                .map(Medicine::fromAcronym)
                .collect(toSet());
    }
}

package com.edgelab.hospital.care;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toUnmodifiableSet;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MedicineSet {

    private static final Set<Set<Medicine>> ALL_SUBSETS = allSubsets(new HashSet<>(Arrays.asList(Medicine.values())));

    public static Set<Medicine> full() {
        return Set.of(Medicine.values());
    }

    public static Set<Set<Medicine>> allSubsetsHaving(Set<Medicine> medicines) {
        return ALL_SUBSETS.stream().filter(it -> it.containsAll(medicines)).collect(toUnmodifiableSet());
    }

    public static Set<Set<Medicine>> allSubsetsNotHaving(Set<Medicine> medicines) {
        return ALL_SUBSETS.stream().filter(it -> Collections.disjoint(it, medicines)).collect(toUnmodifiableSet());
    }

    public static Set<Set<Medicine>> allSubsets(Set<Medicine> having, Set<Medicine> notHaving) {
        return ALL_SUBSETS.stream()
                .filter(it -> it.containsAll(having))
                .filter(it -> Collections.disjoint(it, notHaving))
                .collect(toUnmodifiableSet());
    }

    public static Set<Set<Medicine>> anySubsets() {
        return ALL_SUBSETS;
    }

    public static Set<Medicine> empty() {
        return Set.of();
    }

    private static Set<Set<Medicine>> allSubsets(Set<Medicine> medicineSet) {
        var result = new HashSet<Set<Medicine>>();
        result.add(Set.copyOf(medicineSet));

        for (var medicine : Set.copyOf(medicineSet)) {
            medicineSet.remove(medicine);
            result.addAll(allSubsets(new HashSet<>(medicineSet)));
            medicineSet.add(medicine);
        }
        return result;
    }

}


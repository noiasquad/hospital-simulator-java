package com.edgelab.hospital.io;

import com.edgelab.hospital.care.Medicine;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static com.edgelab.hospital.care.Medicine.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class MedicineSetReaderTest {

    //given
    private MedicineSetReader reader = new MedicineSetReader();

    @Test
    public void should_parse_valid_single_medicine_accordingly() {
        //when-then
        for (var state : Medicine.values()) {
            assertThat(reader.read(state.getAcronym())).isEqualTo(Set.of(state));
        }
    }

    @Test
    public void should_parse_valid_multi_medicine_list_accordingly() {
        //when-then
        assertThat(reader.read(" As,An, I,P ")).isEqualTo(Set.of(Aspirin, Antibiotic, Insulin, Paracetamol));
        assertThat(reader.read(" P,P ")).isEqualTo(Set.of(Paracetamol));
    }

    @Test
    public void should_parse_empty_medicine_list_accordingly() {
        //when-then
        assertThat(reader.read("")).isEqualTo(Set.of());
    }

    @Test
    public void should_detect_illegal_syntax_for_list() {
        //when-then
        assertThatThrownBy(() -> reader.read("As,")).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> reader.read("I;P")).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void should_detect_unknown_medicine() {
        //when-then
        assertThatThrownBy(() -> reader.read("a")).isInstanceOf(IllegalArgumentException.class);
    }
}

package com.edgelab.hospital.care;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import java.util.List;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum HealthStatus {
    Fever("F"),
    Healthy("H"),
    Diabetes("D"),
    Tuberculosis("T"),
    Dead("X");

    private static final List<HealthStatus> LOV = List.of(values());
    @Getter
    @NonNull
    private String acronym;

    public static HealthStatus fromAcronym(String acronym) {
        return LOV.stream()
                .filter(it -> it.acronym.equalsIgnoreCase(acronym))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unknown health status: %s", acronym)));
    }
}

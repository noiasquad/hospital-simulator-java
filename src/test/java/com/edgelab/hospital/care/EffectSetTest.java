package com.edgelab.hospital.care;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static com.edgelab.hospital.care.EffectSet.EligibleEffect.effectOf;
import static com.edgelab.hospital.care.HealthStatus.*;
import static com.edgelab.hospital.care.Medicine.*;
import static java.util.function.UnaryOperator.identity;
import static org.assertj.core.api.Assertions.assertThat;

public class EffectSetTest {

    @Test
    public void if_no_existing_rule_then_elected_effect_is_no_effect() {
        //when-then
        assertThat(EffectSet.of(List.of(), List.of()).electEffect(Healthy, Set.of())).isEqualTo(identity());
    }

    @Test
    public void if_no_match_with_existing_rule_then_elected_effect_is_no_effect() {
        //given
        var safe = effectOf(Set.of(Fever), m -> m.contains(Paracetamol), s -> Healthy);
        var mortal = effectOf(Set.of(Fever), m -> m.containsAll(Set.of(Aspirin, Paracetamol)), s -> Dead);
        var eligibleEffects = EffectSet.of(List.of(mortal), List.of(safe));
        //when-then
        assertThat(eligibleEffects.electEffect(Healthy, Set.of())).isEqualTo(identity());
        assertThat(eligibleEffects.electEffect(Fever, Set.of())).isEqualTo(identity());
        assertThat(eligibleEffects.electEffect(Fever, Set.of(Antibiotic))).isEqualTo(identity());
    }

    @Test
    public void should_elect_a_safe_effect_if_no_eligible_mortal_effect() {
        //given
        var safe1 = effectOf(Set.of(Fever), m -> m.contains(Paracetamol), s -> Healthy);
        var safe2 = effectOf(Set.of(Fever), m -> m.contains(Aspirin), s -> Healthy);
        var mortal = effectOf(Set.of(Fever), m -> m.containsAll(Set.of(Aspirin, Paracetamol)), s -> Dead);
        var eligibleEffects = EffectSet.of(List.of(mortal), List.of(safe1, safe2));
        //when-then
        assertThat(eligibleEffects.electEffect(Fever, Set.of(Aspirin)).apply(Fever)).isEqualTo(Healthy);
        assertThat(eligibleEffects.electEffect(Fever, Set.of(Paracetamol)).apply(Fever)).isEqualTo(Healthy);
    }

    @Test
    public void should_elect_a_mortal_effect_first() {
        //given
        var safe1 = effectOf(Set.of(Fever), m -> m.contains(Paracetamol), s -> Healthy);
        var safe2 = effectOf(Set.of(Fever), m -> m.contains(Aspirin), s -> Healthy);
        var mortal = effectOf(Set.of(Fever), m -> m.containsAll(Set.of(Aspirin, Paracetamol)), s -> Dead);
        var eligibleEffects = EffectSet.of(List.of(mortal), List.of(safe1, safe2));
        //when-then
        assertThat(eligibleEffects.electEffect(Fever, Set.of(Aspirin, Paracetamol)).apply(Fever)).isEqualTo(Dead);
    }
}
